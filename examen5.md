### Examen unidad 5 y 6
Utilizando PROGRAMACION ORIENTADA A OBJETOS. Cree una clase Joyeria que almacenara objetos de tipo Cliente y Empleado en una estructura de datos (arreglo , arraylist, o lo que considere mas conveniente), implemente funciones de creacion , actualizacion y eliminacion (CRUD) tanto para Cliente como para Empleado.

La clase Cliente presenta los siguientes atributos:
- nombre
- apellido paterno
- direccion
- tipo (frecuente,general o platino)


y la clase Empleado tiene los atributos:
- nombre
- apellido paterno
- telefono
- salario mensual
- direccion

los metodos de ambas clases seran los que considere necesarios, ademas de la visibilidad. NO OLVIDE UTILIZAR HERENCIA O IMPLEMENTACION, segun considere mas conveniente.

Regla de negocio:
La Cafeteria DEBE tener al menos un Empleado.
La cafeteria puede tener ninguno o muchos Clientes.

Escriba una aplicación de prueba llamada PruebaJoyeria o Main como mejor prefiera, que demuestre el CRUD de Clientes y Empleados. Ademas de poder ordenar a los clientes por nombre y guardar en un archivo los datos de los empleados. El uso de excepciónes o validaciones quedan a su consideracion.

## Condiciones de entrega
Subira el archivo .jar a su cuenta de github el cual tendra el siguente nombre :
unidad5[numerodecontrol].jar
ejemplo
unidad512161219
ademas del codigo.