class circulo extends punto{

     double radio;

    public circulo(int a, int b,double c){
     	super(a,b);
     	radio = c;
     }

    public double area(){
		return 3.1416*radio*radio;
	}

    public String tostring(){
    	return "centro = "+ super.toString() + " radio= "+ radio ;
    }

    public String obtienenombre(){
    	return "soy un circulo";
    }
}