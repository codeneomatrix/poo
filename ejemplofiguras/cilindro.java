class cilindro extends circulo{

     double altura;

    public cilindro(int a, int b,double c, double d){
        super(a,b,c);
     	altura = d;
     }

	public double area(){
		return 2* super.area() + 2* radio*altura*3.1416;
	}

    public double volumen(){
        return super.area()*altura;
    }

    public String obtienenombre(){
    	return "soy un cilindro";
    }
}