## Reto 1
Cree una clase llamada Pokemon, que incluya tres piezas de información como 
variables de instancia: 
- nombre, 
- nivel de ataque 
- nivel de defensa y
 - atributos que considere convenientes
Proporcione un método establecer y un método obtener para las variables de instancia (atributos) que considere convenientes. 
Si algun valor de nivel (ataque o defensa) no es positivo, establézcalo a 0.0. 

Escriba una aplicación de prueba llamada LigaPokemon o Main como mejor 
prefiera, que permita realizar una batalla pokemon. 
Cree dos objetos Pokemon y realice una batalla entre ellos.

### Condiciones de entrega 
Subira un archivo .jar a su cuenta de github, el cual tendra el siguiente 
nombre: pokemos[numerodecontrol].jar, 
ejemplo: pokemon112161219.jar 
ademas del codigo. 


## Reto 2
Cree una clase Banco que almacenara objetos de tipo Cliente y Empleado en una estructura de datos (arreglo , arraylist, o lo que considere mas conveniente), implemente funciones de creacion , actualizacion y eliminacion (CRUD) tanto para Cliente como para Empleado.

La clase Cliente presenta los siguientes atributos:
- nombre
- apellido paterno
- direccion
- numero de cuenta

 y la clase Empleado tiene los atributos:
- nombre
- apellido paterno
- telefono
- salario mensual

los metodos de ambas clases seran los que considere necesarios, ademas de la visibilidad.

Regla de negocio:
El banco DEBE tener al menos un Empleado.
El banco puede tener ninguno o muchos Clientes.

Escriba una aplicación de prueba llamada PruebaBanco o Main como mejor prefiera, que demuestre el CRUD de Clientes y Empleados.
Ademas de poder ordenar a los Empleados por nombre 
tome en cuenta que Array.sort() solo funciona con arreglos, si desea utilizar arraylist debera imvestigar el nombre del objeto correspondiente,
y guardar en un archivo los datos de los Clientes. 
Tome en cuenta que java.io.Serializable crea archivos binarios o sea que solo puede leer la maquina, en este caso se equerien archivos de texto, los que pueden leer las personas. 
El uso de excepciónes o validaciones quedan a su consideracion.

### Condiciones de entrega
Subira un archivo .jar a su cuenta de github, el cual tendra el siguiente
nombre: banco[numerodecontrol].jar,
ejemplo: banco12161219.jar
ademas del codigo.

## Reto 3
Utilizando PROGRAMACION ORIENTADA A OBJETOS. Cree una clase Joyeria que almacenara objetos de tipo Cliente y Empleado en una estructura de datos (arreglo , arraylist, o lo que considere mas conveniente), implemente funciones de creacion , actualizacion y eliminacion (CRUD) tanto para Cliente como para Empleado.

La clase Cliente presenta los siguientes atributos:
- nombre
- apellido paterno
- direccion
- tipo (frecuente,general o platino)


y la clase Empleado tiene los atributos:
- nombre
- apellido paterno
- telefono
- salario mensual
- direccion

los metodos de ambas clases seran los que considere necesarios, ademas de la visibilidad. NO OLVIDE UTILIZAR HERENCIA O IMPLEMENTACION, segun considere mas conveniente.

Regla de negocio:
La Cafeteria DEBE tener al menos un Empleado.
La cafeteria puede tener ninguno o muchos Clientes.

Escriba una aplicación de prueba llamada PruebaJoyeria o Main como mejor prefiera, que demuestre el CRUD de Clientes y Empleados. Ademas de poder ordenar a los clientes por nombre y guardar en un archivo los datos de los empleados. El uso de excepciónes o validaciones quedan a su consideracion.

ADEMAS DEBERA IMPLEMENTAR LA OPCIONDE BUSQUEDA, EMPLEANDO EL USO DE UNA INTERFAZ YA EXISTENTE EN JAVA (SIMILAR A LO QUE SE HACE EN EL ORDENAMINETO)

## Condiciones de entrega
Subira el archivo .jar a su cuenta de github el cual tendra el siguente nombre :
unidad5[numerodecontrol].jar
ejemplo
unidad512161219
ademas del codigo.


