import java.io.*; //libreria que permite la manipulacion de archivos
import java.util.Arrays;
import javax.swing.JOptionPane;

public class AlumnoArrays {

	static Alumno[] arreAlumno= new Alumno[10];

	public static void main( String args[] ){

	 arreAlumno[0] = new Alumno("cruz del vall","0916966",7);
     arreAlumno[1] = new Alumno("sanchez jimenez","10161007",5);
     arreAlumno[2] = new Alumno("flores jimenez","10161001",5);
     arreAlumno[3] = new Alumno("antonio altamirano","11160875",3);
     arreAlumno[4] = new Alumno("balderas talarico","12161233",1);
     arreAlumno[5] = new Alumno("acevedo maldonado","12161219",1);
     arreAlumno[6] = new Alumno("jesus marquez","12161524",4);
     arreAlumno[7] = new Alumno("memo ruiz","14241512",2);
     arreAlumno[8] = new Alumno("cristina aguilera","12161524",3);
     arreAlumno[9] = new Alumno("briney spears","09087456",4);


     escribeArre();
     Arrays.sort(arreAlumno);
     //escribeArre();
     escribeArre_en_archivo();
     lee_archivo();
 }

 public static void lee_archivo(){
      File archivo ;
      FileReader fr ;
      BufferedReader br ;

      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("alumnos.txt");//("C:\\archivo.txt");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

         // Lectura del fichero
         String linea;
         while((linea=br.readLine())!=null){
                     System.out.println(linea);
         }
          br.close();
          fr.close();

      }
      catch(IOException e){};


 }
 public static void escribeArre_en_archivo(){
     File f;
     f = new File("alumnos.txt");

     int i=0;
     String s="";
     for(i=0;i<arreAlumno.length;i++){
          s += "nombre: " +  arreAlumno[i].nombre+ "\n";
          s += "numerodecontrol: " +  arreAlumno[i].numControl+"\n";
          s += "semestre: " +  arreAlumno[i].semestre+"\n";

     }

     try{
     FileWriter w = new FileWriter(f);
     BufferedWriter bw = new BufferedWriter(w);
     PrintWriter wr = new PrintWriter(bw);

     wr.write("Esta es una linea de codigo");//escribimos en el archivo

     //wr.append(" - y aqui continua"); //concatenamos en el archivo sin borrar lo existente
     wr.append(s);

             //ahora cerramos los flujos de canales de datos, al cerrarlos el archivo quedará guardado con información escrita

             //de no hacerlo no se escribirá nada en el archivo

     wr.close();
     bw.close();
     }catch(IOException e){};

}


 public static void escribeArre(){
     int i=0;
     String s="";
     for(i=0;i<arreAlumno.length;i++){
          s += arreAlumno[i].toString();
     }
     //JOptionPane.showMessageDialog(null,s,"ORDENAMIENTO",JOptionPane.INFORMATION_MESSAGE);
 System.out.println(s);

 }
}




