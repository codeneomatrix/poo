class bicicleta {

	String color;
	private int rodada;
	String marca;
	private int kilometraje;

    bicicleta(String marca, int r, String c){
    	color = c;
    	if (r >= 20) {
    		rodada = r;
    	}else{
    		rodada = 20;
    	}
    	this.marca = marca;
    	kilometraje = 0;
    }

	public void arrancar(){
		System.out.println("la bicicleta marca "+ marca+ "   esta arrancando");
	}
	public void establecerrodada(int ro){
       if (ro>0) {
       	this.rodada = ro;
       }
	}

	public int obtenerrodada(){
      return rodada;
	}

	public int obtenerkilometraje(){
		return kilometraje;
	}

	public void avanzar(){
		kilometraje++;
	}

	public void detener(){
		System.out.println("la bicicleta marca "+ marca+ "se esta deteniendo");
	}
	public void cambio(){
		System.out.println("la bicicleta marca "+ marca+ "esta cambiando la velocidad");
	}


}