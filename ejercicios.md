# Ejercicio 1

Cree una clase **Rectangulo**. La clase rectangulo tienen los siguentes atributos.
  - longitud
  - ancho

Cada uno con el valor predeterminado de 1. Tiene dos metodos uno calcula el **perimetro** y el otro el  **area** del rectangulo; la clase tambien tiene metodos para *modificar* y *obtener* los valores de la **longitud** y el **ancho**. Los valores que admiten **longitud** y **ancho** debe ser iguales o mayores a 1.0 y menores que 20.0.


# Ejercicio 2

Cree una clase llamada **Racional** para realizar operaciones aritmeticas con fracciones, utilice las variables **numerador** y **denominador**. Proporcione valores predeterminados para ambas variables y un metodo para modificar cada una de ellas. Proporcione metodos para realizar:
- *suma* de dos numeros racionales  y
- *resta* de dos numeros racionales
- impresion en pantalla del numero racional en la forma **a/b**, donde **a** es el **numerador** y **b** es el **denominador**.

*(Nota: se puede elegir cualquiera de los dos ejemplos )*