class lavadora{
	public String marca;
	public String serie;
	public String color;
	private int tiempo_lavado;
	private int fuerza_lavado;
	public int tamanio;
	private float  capacidad;

	public lavadora(String marca,String serie,String color, int tamanio, float c){
     this.marca = marca;
     capacidad = c;
     this.serie = serie;
     this.color = color;
     this.tamanio = tamanio;
     tiempo_lavado = 0;
     fuerza_lavado = 0;
	}

public void  sensibilidad(String tipo_ropa){
	if (tipo_ropa.equals("mezclilla")) {
		tiempo_lavado = 10;
		fuerza_lavado = 5;
	}
	if (tipo_ropa.equals("algodon")) {
		tiempo_lavado = 5;
		fuerza_lavado = 2;
	}
	if (tipo_ropa.equals("seda")) {
		tiempo_lavado = 2;
		fuerza_lavado = 1;
	}
}
public void  lavar( float cantidad){
    if (cantidad <= capacidad) {
       	System.out.println("se esta lavando la ropa con el tiempo "+ tiempo_lavado+ " y una fuerza de "+ fuerza_lavado);
       }else{
       	System.out.println("se ha superado la capacidad maxima ");
       }
}
public void  remojar(){
System.out.println("la lavadora marca "+marca+ " esta remojando");
}
public void  secar(){
System.out.println("la lavadora marca "+marca+ " esta secando");
}
public void  enjuagar(){
System.out.println("la lavadora marca "+marca+ " esta enjuagando");
}

}