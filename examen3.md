## Examen unidad 3 y 4

Cree una clase Cafeteria que almacenara objetos de tipo Cliente y Empleado en una estructura de datos (arreglo , arraylist, o lo que considere mas conveniente), implemente funciones de creacion , actualizacion y eliminacion (CRUD) tanto para Cliente como para Empleado.

La clase Cliente presenta los siguientes atributos:
- nombre
- apellido paterno
- direccion
- tipo (frecuente,general o platino)

 y la clase Empleado tiene los atributos:
- nombre
- apellido paterno
- telefono
- salario mensual
- direccion

los metodos de ambas clases seran los que considere necesarios, ademas de la visibilidad.

Regla de negocio:
La Cafeteria DEBE tener al menos un Empleado.
La cafeteria puede tener ninguno o muchos Clientes.

Escriba una aplicación de prueba llamada PruebaCafeteria o Main como mejor prefiera, que demuestre el CRUD de Clientes y Empleados.


### Condiciones de entrega
Subira un archivo .jar a su cuenta de github, el cual tendra el siguiente
nombre: unidad3[numerodecontrol].jar,
ejemplo: unidad312161219.jar
ademas del codigo.