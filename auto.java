class auto{
	String modelo;
	String marca;
	String color;
	private int numero_de_puertas;
	boolean tipo;
  private int kilometros;

   auto(String modelo, String mo, String c, boolean t, int np){
      this.modelo = modelo;
      marca = mo;
      tipo = t;
      color = c;
      numero_de_puertas = np;
      kilometros = 0;
   }

	public void avanzar(){
		System.out.println("El auto modelo "+ modelo+ " marca "+ marca+ " esta avanzando");
    kilometros++;
       }

    public void colocarpuertas(int np){ //set
    	if (np>0) {
    		this.numero_de_puertas = np;
    	}

    }

    public int obtenerpuertas(){ // get
      return numero_de_puertas;
    }

    public int obtenerkilometros(){
      return kilometros;
    }

		public void detenerse(){
			System.out.println("El auto modelo "+ modelo+ " marca "+ marca+ " se esta deteniendo");
		}


	}

