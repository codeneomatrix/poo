class nintendo{

	public String modelo;
	public  boolean estado;
	public String color;
	private String juegoactual;
	public boolean portatil;
    private int capacidad;
    private String numerodeserie;
    private int partidas;
    private int versionsoftware;

    public nintendo(String modelo, String color, Boolean  portatil, int capacidad, String numerodeserie){
        this.modelo = modelo;
        this.color = color;
        this.numerodeserie = numerodeserie;
        this.capacidad = capacidad;
        this.portatil = portatil;
        estado = false;
        partidas = 0;
    }

    public void resetear(){
      if (estado) {
        partidas = 0;
       System.out.println("se se  reseteo el nintendo");
      }else{
        System.out.println("no se puede resetear porque esta apagado");
      }

    }

    public void descargar( String juego){
       if (estado) {
         System.out.println("se esta descargando el juego "+ juego);
       }else{
        System.out.println("enciende el nintendo ");
       }

    }

    public void guardarpartida(){
      if (estado) {
        if ((partidas+1)<=capacidad) {
          partidas++;
       System.out.println("partida guardada numero "+ partidas+" del juego "+juegoactual);
        }else{
          System.out.println("la memoria se lleno  ");
        }

      }else{
        System.out.println(" prende la consola primero ");
      }

    }

    public void borrar(){
      if (estado) {
        if (partidas >0) {
        partidas--;
          System.out.println("partida borrada del juego "+juegoactual+ "quedan "+partidas +"  partidas");
      }
      }else{
        System.out.println("no se puede borrar partida, el nintendo esta apagado ");
      }

    }

    public void actualizarse(){
      if (estado) {
        versionsoftware++;
       System.out.println("la consola se ha actualizado a la version "+versionsoftware);
      }else{
        System.out.println("la consola no se ha actualizado, primero prendela ");
      }

    }
    public void jugar( String j){

      if (estado) {
        juegoactual = j;
       System.out.println("el juego actual "+ juegoactual+ "  se inicio");
      }else{
        System.out.println(" prende la consola primero ");

    }
      }
}