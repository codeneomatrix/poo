import java.io.*; 
import javax.swing.JOptionPane;
  class FileReadTest { 
	String s="";
     public static void main (String[] args) { 
        FileReadTest f = new FileReadTest();
        f.readMyFile(); 
     } 

     void readMyFile() { 

        DataInputStream dis = null; 
        String record = null; 
        int recCount = 0; 

        try { 

           File f = new File("Estudiante.txt"); 
           FileInputStream fis = new FileInputStream(f); 
           BufferedInputStream bis = new BufferedInputStream(fis); 
           dis = new DataInputStream(bis);  

           while ( (record=dis.readLine()) != null ) { 
              recCount++; 
              	
               s += record+"\n"; 
           } 
           	JOptionPane.showMessageDialog(null, recCount + ": " + s, "POEMA",JOptionPane.ERROR_MESSAGE);

        } catch (IOException e) {  
           System.out.println("Uh oh, error IOException!" + e.getMessage()); 

        } finally { 
           if (dis != null) { 
              try {
                 dis.close(); 
              } catch (IOException ioe) {
                 System.out.println("Error de �ltimo momento!");
              }
           } 
        } 
     } 
}

