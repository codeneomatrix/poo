public class triangulo extends poligono{


public triangulo(double base, double altura){
  super(base, altura);
}

public double area(){
	return (base*altura)/2;
}

public double perimetro(){
	return base+altura+(Math.sqrt((base*base)+(altura*altura)));
}

}