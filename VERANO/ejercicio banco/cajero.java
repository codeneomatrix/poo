import javax.swing.JOptionPane;
import java.util.*;
import java.io.*;
public class cajero{

lectorhuellas lector;
ArrayList<cuenta>  cuentas_bancarias = new ArrayList<cuenta>();



public void leercuentas_objeto(){
try{
  FileInputStream fis = new FileInputStream("cuentas.dat");
  ObjectInputStream ois = new ObjectInputStream(fis);

   Object elemento;
  while((elemento=ois.readObject())!=null){
      cuentas_bancarias.add((cuenta) elemento);
         }

  ois.close();
  fis.close();

}catch(Exception e){
  //e.printStackTrace();
}
}

public void guardarcuentas_objeto(){
	try{
    FileOutputStream fs = new FileOutputStream("cuentas.dat");//Creamos el archivo
    ObjectOutputStream os = new ObjectOutputStream(fs);//Esta clase tiene el método writeObject() que necesitamos
    for (int i=0;i<cuentas_bancarias.size();i++ ) {
		os.writeObject(cuentas_bancarias.get(i));
		//El método writeObject() serializa el objeto y lo escribe en el archivo
	}

    os.close();//Hay que cerrar siempre el archivo
  }catch(FileNotFoundException e){
    e.printStackTrace();
  }catch(IOException e){
    e.printStackTrace();
  }
}


public void crearcuenta(){
	int eleccion = Integer.parseInt(JOptionPane.showInputDialog("Bienvenido \n ¿Que tipo de cuenta desea crear? \n 1.*-cuenta normal \n 2.*-cuenta prospera"));

	int montoinicial = Integer.parseInt(JOptionPane.showInputDialog("ingrese el monto inicial"));

     if (eleccion ==1) {
     	cuentas_bancarias.add(new cuentanormal(montoinicial,"td553f"));
     }
	if (eleccion== 2) {
        cuentas_bancarias.add(new cuentaprospera(montoinicial,"td553f"));
	}

	guardarcuentas_objeto();
}

public void eliminarcuenta(){
	int indice = Integer.parseInt(JOptionPane.showInputDialog("¿Cual es su cuenta bancaria?"));

	cuentas_bancarias.remove(indice);
}


public void mostrarsaldo( int indice){

System.out.println(cuentas_bancarias.get(indice));
}

public void menu(){

	leercuentas_objeto();
	while(true){

	int eleccion = Integer.parseInt(JOptionPane.showInputDialog("Bienvenido \n ¿que desea hacer? \n 1.*-crearcuenta \n 2.*-usar cuenta \n 3.*- eliminar cuenta"));
     if (eleccion ==1) {
     	crearcuenta();
     }
	if (eleccion== 2) {
		usarcuenta();
	}
	if (eleccion==3) {
		eliminarcuenta();
	}
	}
}

public void usarcuenta(){
int indice = Integer.parseInt(JOptionPane.showInputDialog("¿Cual es su cuenta bancaria?"));

int eleccion = Integer.parseInt(JOptionPane.showInputDialog("Bienvenido \n ¿que desea hacer? 1.*- mostrar saldo\n 2.*-hacer retiro \n 3.*-hacer deposito"));

     if (eleccion==1) {
     	mostrarsaldo(indice);
     }
	if (eleccion==2) {
      hacerretiro(indice);
	}
	if (eleccion==3) {
		hacerdeposito(indice);
	}

}

public void hacerdeposito(int indice){
  int cantidad = Integer.parseInt(JOptionPane.showInputDialog("Bienvenido \n ¿cuanto desea depositar?" ));

  cuentas_bancarias.get(indice).meterdinero(cantidad);
guardarcuentas_objeto();
}

public void hacerretiro(int indice){
	int cantidad = Integer.parseInt(JOptionPane.showInputDialog("Bienvenido \n ¿cuanto desea retirar?" ));
	if ((cuentas_bancarias.get(indice).saldo>= cantidad) && (cantidad>0)) {
	cuentas_bancarias.get(indice).sacardinero(cantidad);
	System.out.println("retiro exitoso, toma tus $"+cantidad);
	}else{
		System.out.println("No seas ratero!!!");
	}
	guardarcuentas_objeto();
}


}