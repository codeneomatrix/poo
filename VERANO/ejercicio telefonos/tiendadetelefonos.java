import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.*;
import java.io.*;
public class tiendadetelefonos{

public  ArrayList<telefono>  conjunto_telefonos = new ArrayList<telefono>();

public void creartelefono(){
int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que telefono desea crear?\n 1.- crear movil \n 2.- crear decalle \n 3.- crear telefono fijo"));
		if (eleccion==1) {
			conjunto_telefonos.add(new celular());
		}
		if (eleccion==2) {
			conjunto_telefonos.add(new decalle());
		}
		if (eleccion==3) {
			conjunto_telefonos.add(new fijo());
		}

}


public void guardartelefono_plano(){
     File f = new File("telefonos.txt");

     String s="";

   for (int j =0;j<conjunto_telefonos.size();j++){
	s+=conjunto_telefonos.get(j).toString();
	s+="\n";
    }

     try{
     FileWriter w = new FileWriter(f);
     BufferedWriter bw = new BufferedWriter(w);
     PrintWriter wr = new PrintWriter(bw);

     wr.write(s);//escribimos en el archivo

     //wr.append(" - y aqui continua"); //concatenamos en el archivo sin borrar lo existente
     //wr.append(s);

             //ahora cerramos los flujos de canales de datos, al cerrarlos el archivo quedará guardado con información escrita

             //de no hacerlo no se escribirá nada en el archivo

     wr.close();
     bw.close();
     }catch(IOException e){};

}


public void lee_archivo_telefonos(){
      File archivo ;
      FileReader fr ;
      BufferedReader br ;

      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("telefonos.txt");//("C:\\archivo.txt");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

         // Lectura del fichero
         String linea;
         while((linea=br.readLine())!=null){
            System.out.println(linea);
         }
          br.close();
          fr.close();

      }
      catch(IOException e){};
 }


public void leertelefono_objeto(){
try{
  FileInputStream fis = new FileInputStream("telefono.al");
  ObjectInputStream ois = new ObjectInputStream(fis);

  //El método readObject() recupera el objeto
  Object elemento;
while((elemento=ois.readObject())!=null){
      conjunto_telefonos.add((telefono) elemento);
   }

  ois.close();
  fis.close();
}catch(Exception e){
  //e.printStackTrace();
}
}

public void guardartelefono_objeto(){
	try{
    FileOutputStream fs = new FileOutputStream("telefono.al");//Creamos el archivo
    ObjectOutputStream os = new ObjectOutputStream(fs);//Esta clase tiene el método writeObject() que necesitamos
    for (int i=0;i<conjunto_telefonos.size();i++ ) {
		os.writeObject(conjunto_telefonos.get(i));
	//El método writeObject() serializa el objeto y lo escribe en el archivo
    }

    os.close();//Hay que cerrar siempre el archivo
  }catch(FileNotFoundException e){
    e.printStackTrace();
  }catch(IOException e){
    e.printStackTrace();
  }
}

public void mostrartelefonos(){

for (int i =0;i<conjunto_telefonos.size() ;i++ ) {
	System.out.println("indice : "+ i +"\n");
	System.out.println(conjunto_telefonos.get(i));
}
guardartelefono_objeto();
guardartelefono_plano();
}

public void actualizartelefono(){
int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que telefono desea actualizar"));
conjunto_telefonos.get(eleccion).precio = Integer.parseInt(JOptionPane.showInputDialog("¿Cual es el nuevo precio del telefono"));
}
public void eliminartelefono(){
int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que telefono desea eliminar"));
conjunto_telefonos.remove(eleccion);
}
public void usartelefono(){
int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que telefono desea usar"));
conjunto_telefonos.get(eleccion).llamar();
}


public void menuprincipal(){
	leertelefono_objeto();
	lee_archivo_telefonos();
	while(true){
		int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que desea hacer?\n 1.- crear telefono \n 2.- actualizar telefono \n 3.- eliminar telefono \n 4.- usar un telefono\n 5.- mostrar los telefonos"));
		if (eleccion==1) {
			creartelefono();
		}
		if (eleccion==2) {
			actualizartelefono();
		}
		if (eleccion==3) {
			eliminartelefono();
		}
		if (eleccion==4) {
			usartelefono();
		}
		if (eleccion==5) {
			mostrartelefonos();
		}
}

}


public static void main( String args[] ){
tiendadetelefonos tiendita1 = new tiendadetelefonos();
tiendita1.menuprincipal();
}


}