Cree una clase Cafeteria que almacenara objetos de tipo Cliente y Empleado en una estructura de datos (arreglo , arraylist, o lo que considere mas conveniente), implemente funciones de creacion , actualizacion y eliminacion (CRUD) tanto para Cliente como para Empleado.
La clase Cliente presenta los siguientes atributos:

nombre
apellido paterno
direccion
tipo (frecuente,general o platino)

y la clase Empleado tiene los atributos:

nombre
apellido paterno
telefono
salario mensual
direccion

los metodos de ambas clases seran los que considere necesarios, ademas de la visibilidad.

Utilice los archivos serializados para almacenar los datos de los Clientes y Empleados ademas de los archivos de texto plano.



Regla de negocio:
La Cafeteria DEBE tener al menos un Empleado.
La Cafeteria puede tener ninguno o muchos Clientes.

