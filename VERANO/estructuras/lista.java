public class lista{

int lista[];
int indicecabeza=0;
int tamaniototal;
public lista(int tamanio){
	tamaniototal = tamanio;
lista = new  int[tamanio];
}

public void add(int valor){
	lista[indicecabeza]=valor;
	indicecabeza++;
	if (indicecabeza==tamaniototal) {
		indicecabeza=0;
	}
}

public boolean hasnext(){
	boolean indiceactual  = indicecabeza<=(tamaniototal-1);
	if (indicecabeza==tamaniototal) {
		indicecabeza=(tamaniototal-1);
	}

	return indiceactual;
}

public boolean hasprev(){
	boolean indiceactual= indicecabeza>=0;
	if (indicecabeza<0) {
		indicecabeza=0;
	}
	return indiceactual;
}

public int next(){
	int dato = lista[indicecabeza];
	indicecabeza= indicecabeza+1;
	if (indicecabeza==tamaniototal) {
		indicecabeza=0; //esto sole es para las circulares
	}
return dato;
}

public int prev(){
	int dato = lista[indicecabeza];
	indicecabeza= indicecabeza-1;
	if (indicecabeza==0) {
		indicecabeza=(tamaniototal-1); //esto sole es para las circulares
	}
return dato;
}

}