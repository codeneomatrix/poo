import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.*;
import java.io.*;
class angar {
 ArrayList<volador>  conjunto_naves = new ArrayList<volador>();

public void buscarnave(){
   volador busqueda = new volador("volador");

    for (int i=0;i<conjunto_naves.size();i++ ) {
    	if (busqueda.equals(conjunto_naves.get(i))==true) {
          conjunto_naves.get(i).informacion();
    	}
    }

}


public void ordenarnaves(){
	for (int j=0;j<conjunto_naves.size();j++ ) {

	for (int i=0;i<conjunto_naves.size()-1;i++ ) {

		if (conjunto_naves.get(i).compareTo(conjunto_naves.get(i+1))== 1 ) {
		volador temporal = conjunto_naves.get(i);
		conjunto_naves.set(i,conjunto_naves.get(i+1));
		conjunto_naves.set(i+1,temporal);
	}
   }
}
}

public void mostrarnaves(){
	for (int i=0;i<conjunto_naves.size();i++ ) {
	System.out.println("\n indice: "+i);
	conjunto_naves.get(i).informacion();
}
//ordenarnaves();
Collections.sort(conjunto_naves);
}


public void actualizarnave(){
int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que nave quiere actualizar?"));
    conjunto_naves.get(eleccion).actualizarprecio();
}


public void eliminarnave(){
int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que nave quiere borrar?"));
	conjunto_naves.remove(eleccion);
}


public void lee_archivo(){
      File archivo ;
      FileReader fr ;
      BufferedReader br ;

      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("naves.txt");//("C:\\archivo.txt");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

         // Lectura del fichero
         String linea;
         while((linea=br.readLine())!=null){
            System.out.println(linea);

         }
          br.close();
          fr.close();

      }
      catch(IOException e){};


 }


public void leernave_objeto(){
try{
  FileInputStream fis = new FileInputStream("naves.nav");
  ObjectInputStream ois = new ObjectInputStream(fis);
  conjunto_naves.add((volador) ois.readObject());//El método readObject() recupera el objeto
  conjunto_naves.add((volador) ois.readObject());
  ois.close(); 
  fis.close();
}catch(FileNotFoundException e){
  e.printStackTrace();
}catch(IOException e){
  e.printStackTrace();
}catch(ClassNotFoundException e){
  e.printStackTrace();
}
}

public void guardarnave_objeto(){
	try{
    FileOutputStream fs = new FileOutputStream("naves.nav");//Creamos el archivo
    ObjectOutputStream os = new ObjectOutputStream(fs);//Esta clase tiene el método writeObject() que necesitamos
    for (int i=0;i<conjunto_naves.size();i++ ) {

	os.writeObject(conjunto_naves.get(i));
	//El método writeObject() serializa el objeto y lo escribe en el archivo
}

    os.close();//Hay que cerrar siempre el archivo
  }catch(FileNotFoundException e){
    e.printStackTrace();
  }catch(IOException e){
    e.printStackTrace();
  }
}

public void guardarnave_plano(){
     File f;
     f = new File("naves.txt");

     int i=0;
     String s="";
   for (int j=0;j<conjunto_naves.size();j++ ) {
	s+= conjunto_naves.get(j).datos();
    }

     try{
     FileWriter w = new FileWriter(f);
     BufferedWriter bw = new BufferedWriter(w);
     PrintWriter wr = new PrintWriter(bw);

     wr.write("");//escribimos en el archivo

     //wr.append(" - y aqui continua"); //concatenamos en el archivo sin borrar lo existente
     wr.append(s);

             //ahora cerramos los flujos de canales de datos, al cerrarlos el archivo quedará guardado con información escrita

             //de no hacerlo no se escribirá nada en el archivo

     wr.close();
     bw.close();
     }catch(IOException e){};

}


public void crearnaves(){
	int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que quiere crear?\n 1.- avioneta\n 2.- helicoptero\n 3.- jet"));
	if (eleccion==1) {

		conjunto_naves.add(new avioneta());
	}
	if (eleccion==2) {
		conjunto_naves.add(new helicoptero());
	}
	if (eleccion==3) {
		conjunto_naves.add(new jet());
	}

guardarnave_plano();
guardarnave_objeto();
}


public angar(){

leernave_objeto();

	while(true){
	int eleccion = Integer.parseInt(JOptionPane.showInputDialog("¿Que quiere hacer?\n 1.- mostrar naves\n 2.- crear nave\n 3.- eliminar nave \n 4.- actualizar nave \n 5.- buscarnave"));

switch(eleccion){
	case 1: mostrarnaves(); break;
	case 2: crearnaves(); break;
	case 3: eliminarnave(); break;
	case 4: actualizarnave(); break;
	case 5: buscarnave(); break;
}

}


}

public static void main( String args[] ){
angar an = new angar();
}

}
