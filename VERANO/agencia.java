import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.io.*;
class agencia{

	//auto b[] = new auto[2];
ArrayList<auto> b = new ArrayList<auto>();


public void borrarcarro(){
	int posicion = Integer.parseInt(JOptionPane.showInputDialog("¿Que carro quieres borrar?:"));
	b.get(posicion).valido= false;


	guardarcarro_objeto();
}

public void actualizarprecio(){

	int posicion = Integer.parseInt(JOptionPane.showInputDialog("¿Que carro quieres modificar su precio?:"));
	double nuevoprecio= Double.parseDouble(JOptionPane.showInputDialog(" $ "));

	b.get(posicion).precio= nuevoprecio;

}


public void guardarcarro_plano(){
     File f = new File("carros.txt");

     String s="";

   for (int j =0;j<b.size() ;j++ ) {
		if (b.get(j).valido == true) {
	s+="Modelo: "+b.get(j).modelo+ "\n color: "+b.get(j).color+ "\n precio: "+b.get(j).precio;
		}
    }

     try{
     FileWriter w = new FileWriter(f);
     BufferedWriter bw = new BufferedWriter(w);
     PrintWriter wr = new PrintWriter(bw);

     wr.write("");//escribimos en el archivo

     //wr.append(" - y aqui continua"); //concatenamos en el archivo sin borrar lo existente
     wr.append(s);

             //ahora cerramos los flujos de canales de datos, al cerrarlos el archivo quedará guardado con información escrita

             //de no hacerlo no se escribirá nada en el archivo

     wr.close();
     bw.close();
     }catch(IOException e){};

}


public void lee_archivo_carros(){
      File archivo ;
      FileReader fr ;
      BufferedReader br ;

      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("carros.txt");//("C:\\archivo.txt");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

         // Lectura del fichero
         String linea;
         while((linea=br.readLine())!=null){
            System.out.println(linea);
         }
          br.close();
          fr.close();

      }
      catch(IOException e){};
 }


public void actualizarmodelo(){

	int posicion = Integer.parseInt(JOptionPane.showInputDialog("¿Que carro quieres modificar su modelo?:"));
	String nuevomodelo= JOptionPane.showInputDialog("Modelo ");

	b.get(posicion).modelo= nuevomodelo;

}

public void actualizarcolor(){

	int posicion = Integer.parseInt(JOptionPane.showInputDialog("¿Que carro quieres modificar su color?:"));
	String nuevocolor= JOptionPane.showInputDialog("Color: ");

	b.get(posicion).color= nuevocolor;

}

public void creaautos(){


	String color = JOptionPane.showInputDialog("Color:");
	String modelo = JOptionPane.showInputDialog("Modelo:");
    Double precio =Double.parseDouble(JOptionPane.showInputDialog("precio"));

   b.add(new auto(color,modelo,precio));

   guardarcarro_plano();
   lee_archivo_carros();
   guardarcarro_objeto();
}

public void guardarcarro_objeto(){
	try{
    FileOutputStream fs = new FileOutputStream("carros.car");//Creamos el archivo
    ObjectOutputStream os = new ObjectOutputStream(fs);//Esta clase tiene el método writeObject() que necesitamos
    for (int i=0;i<b.size();i++ ) {

	os.writeObject(b.get(i));
	//El método writeObject() serializa el objeto y lo escribe en el archivo
    }

    os.close();//Hay que cerrar siempre el archivo
  }catch(FileNotFoundException e){
    e.printStackTrace();
  }catch(IOException e){
    e.printStackTrace();
  }
}


public void leercarro_objeto(){
try{
  FileInputStream fis = new FileInputStream("carros.car");
  ObjectInputStream ois = new ObjectInputStream(fis);

  //El método readObject() recupera el objeto
  Object elemento;
while((elemento=ois.readObject())!=null){
      b.add((auto) elemento);
         }

  ois.close();
  fis.close();
}catch(Exception e){
  //e.printStackTrace();
}
}


public void mostrarautos(){

	for (int j =0;j<b.size() ;j++ ) {
		if (b.get(j).valido == true) {
	System.out.println("Modelo: "+b.get(j).modelo+ "\n color: "+b.get(j).color+ "\n precio: "+b.get(j).precio);
		}
    }

}

public  agencia(){
	leercarro_objeto();

	while(true){
		int posicion = Integer.parseInt(JOptionPane.showInputDialog("Menu:\n 1.- crearautos\n2.- mostrar autos\n 3.- actualizar precio auto\n 4.- actualizar color auto\n 5.- actualizar modelo auto\n 6.- borrar auto"));


		if (posicion==1) {
		creaautos();
		}

		if (posicion==2) {
		mostrarautos();
		}

		if (posicion==3) {

		actualizarprecio();
		}
		if (posicion==4) {

		actualizarcolor();
		}
		if (posicion==5) {

		actualizarmodelo();
		}
		if (posicion==6) {

		borrarcarro();
		}

	}
}

public static void main( String args[] ){
	agencia a = new agencia();
}

}



